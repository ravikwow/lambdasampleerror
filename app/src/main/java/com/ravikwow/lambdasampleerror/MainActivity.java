package com.ravikwow.lambdasampleerror;

import android.app.Activity;
import android.os.Bundle;
import android.util.Pair;
import android.widget.TextView;

import rx.Observable;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Pair pair = new Pair<>("Hello", " World!");
        Observable.just(pair)
                .map(this::convert)
                .subscribe(this::setText);
    }

    private String convert(Pair<String, String> pair) {
        return pair.first + pair.second;
    }

    private void setText(String text) {
        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText(text);
    }
}
